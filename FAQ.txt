Phishing Boat is a Google Chrome extension that offers protection against most
phishing attacks. The text below is both an FAQ and this extension's Terms of
Service and Privacy Policy. USING OR DOWNLOADING THIS SOFTWARE IMPLIES ACCEPTANCE OF THESE TERMS OF
SERVICE AND PRIVACY POLICY.

Q: How does it work?

A: When you click on a link, Phishing Boat will determine whether you have visited that website recently (since the last extension update and after the "learning period", which is described below). If you haven't, then Phishing Boat will ask you to confirm that you want to proceed to the website before continuing. If you recognize the website, then you can click "OK" to proceed to the website. Otherwise, you can click "Cancel" to remain on the current webpage and avoid traveling to the unfamiliar website.

Q: What is the learning period?

A: The learning period is an amount of time after you first install the extension. For this amount of time (one week), any link you click will be assumed to be a familiar website. In this way, the extension will attempt to "learn" what websites you are used to visiting. After the learning period passes, any unfamiliar links will result in a confirmation prompt similar to the one described above.

Q: How does that help stop phishing attacks?

A: Most people only visit a few dozen distinct websites, and because most phishing attacks rely on a distracted person visiting a website with an intentionally-misspelled name (such as "twiter.com" instead of "twitter.com"), Phishing Boat helps people become more cognizant when they are doing something risky and helps remove the haste that hackers need for their phishing attacks to work. This way, you can stop phishing attacks before they have a chance to do any real damage.

Q: What does Phishing Boat Email do with my email information?

A: Please see the "PRIVACY POLICY" section below.

Q: Does Phishing Boat know which websites are dangerous and which websites are not?

A: Not always. In the learning period, links to untrustworthy websites will be assumed to be trustworthy. One should still utilize diligence when clicking on links to websites, especially when these links redirect to content created by unaffiliated third parties, such as electronically shared documents.

Q: Is this extension guaranteed to stop all phishing attacks?

A: Unfortunately, no. Phishing attacks are constantly evolving and are not rigorously defined. In addition, if the user decides to travel to a website after receiving Phishing Boat's warning, then there is nothing more that Phishing Boat can do to protect the user from any consequences of visiting the website in question. In addition, failure to turn on JavaScript or heed the warnings this software provides in the form of JavaScript alerts may cause this extension to malfunction. Additionally, bugs in this extension, while rare, are virtually impossible to prevent entirely and this extension relies on Google Chrome being secure in order to function optimally.

Futhermore, installing or using this extension implies acceptance of the information in this FAQ and this disclaimer:

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Q: So if it is not perfect, then why should I use this extension? 

A: In spite of all this, Phishing Boat offers you the ability to automatically detect most phishing attacks and alert you before they can do any damage, saving you and your organization from the often catastrophic impacts of a phishing attack. It is not perfect, but it is pretty close!

PRIVACY POLICY:

Q: What information does Phishing Boat share?

A: All information used by Phishing Boat is stored on your device. It is not shared with anyone for any reason.

Q: What information does Phishing Boat Email collect?

A: The information that Phishing Boat collects is limited to the time that you installed the extension plus the domain names of the websites you visited after installing the extension.

Q: How does Phishing Boat Email use this information?

A: This information is solely used for Phishing Boat to tell you whether you should be warned about a website or not (in particular, it is used to determine whether you have visited a website before and whether the extension is still in its learning period). It is not used for any other purposes, such as marketing or data reselling.
