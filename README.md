# README #

### What is Phishing Boat? ###

Phishing Boat is a Google Chrome extension that offers protection
against most phishing attacks.

### How does it work? ###

The basic idea is that, if one clicks on a link to a website that
she has not been to in the past year, Phishing Boat will ask her to
check the url for spelling errors (such as "youtub.com" instead of
"youtube.com") and give her a chance to stay on her current webpage.
If the user decides that there are no spelling errors, then she will
be allowed to continue on to the website. If the user decides that
there are spelling errors, then she will stay on her current page.

### How does that help stop phishing attacks? ###

Because most computer users only visit a handful of websites on a
daily basis, and because most phishing attacks rely on the victim
hastily misreading an intentionally-misspelled URL, asking the user
to re-read the domain name will uncover most phishing scams before
they can start.

### How do I get set up? ###

Go to (insert google chrome extensions store url here) and click
install. Currently, this extension is available only for Google
Chrome. Support for other browsers may be added as schedules
permit.

### Contribution guidelines ###

If submitting a change, please submit a pull request that briefly
explains your change. If submitting an issue, then please
describe which website you attempted to visit (the URL of the link
you clicked on will be fine) and which webpage you currently were
on (the URL in your browser bar will be fine). Without this, we
usually cannot address your issue fully. If possible, provide
information on any webpage you had previously visited, the date
and time when you experienced this issue, and which operating system
you were using.

### Who do I talk to? ###

Andrew Zuelsdorf is the repository manager and the creator of this
software. You can reach him at azuelsdorf16@gmail.com.
