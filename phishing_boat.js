// Copyright (c) 2017 Andrew Zuelsdorf. All rights reserved.

var cookies = {};

const isChrome = false;

const learningPeriodTimeDays = 14;

const learningPeriod = 86400 * learningPeriodTimeDays * 1000;

const MAX_VALUE = Number.MAX_SAFE_INTEGER;

const CUTOFF = 2;

const TOP_LEVEL_DOMAINS = ['.org', '.com', '.net', ".co", '.gov', '.io', '.xxx', '.xyz', '.biz', '.us', '.cn', '.uk', '.ir', '.mx', '.sh', '.in', '.ru', '.il', '.it', '.fr', '.es', '.de', '.pl', '.ie', '.se', '.fi', '.no', '.au', '.nz', '.br', '.gr', '.tr', '.sy', '.bg', '.rs', '.cz', '.name', '.mk', '.ro', '.lt', '.ee', '.sk', '.hr', '.info', '.at', '.jp', '.kr', '.tw', '.ve', '.cu', '.ca', '.si', '.me', '.mo', '.hk'];

// NOTE: all values and keys must be lower-case to ensure accurate mapping
// with strings that are passed in and then lowercased. Also, no string used
// as a key here should also be used as a value.
const SIMILAR_CHAR_MAP = {'l': '1', 'i': '1', 'j': '1', '5': 'b', '6': 'b', '3': '8', '0': 'o'};

function fill_array(array, start_idx, end_idx, value) {
    if (array == null) {
        return;
    }

    for (let i = Math.max(start_idx, 0); i < Math.min(end_idx, array.length); ++i) {
        array[i] = value;
    }
}

function fill_whole_array(array, value) {
    fill_array(array, 0, array.length, value);
}

function normalize_domain(domain) {
    // do not attempt to normalize IPs.
    if (validateIPAddress(domain)) {
        console.log("Found IP address. Returning with no normalization.");
        return domain;
    }

    // Lowercase since URLs don't respect casing.
    let domain_sanitized = domain.toLowerCase();

    for (let i = 0; i < TOP_LEVEL_DOMAINS.length; ++i) {
        if (domain_sanitized.endsWith(TOP_LEVEL_DOMAINS[i])) {
            domain_sanitized = domain_sanitized.slice(0, domain_sanitized.lastIndexOf(TOP_LEVEL_DOMAINS[i]));

	    i = 0;
	}
    }

    // remove dashes and periods as they don't add information.
    domain_sanitized = domain_sanitized.replace(/\./g, '').replace(/\-/g, '');

    let new_domain = [];

    // combine visually similar letters and numbers.
    for (let i = 0; i < domain_sanitized.length; ++i) {
        new_domain.push(SIMILAR_CHAR_MAP[domain_sanitized[i]] || domain_sanitized[i]);
    }

    return new_domain.join('');
}

function unlimited_compare(left, right) {
    if (left == null || right == null) {
        throw new Error("strings must not be null");
    }

    return limited_compare(left, right, Math.max(left.length, right.length));
}

function unlimited_compare_norm(left, right) {
    return unlimited_compare(normalize_domain(left), normalize_domain(right));
}

function limited_compare(left, right, threshold) {
    if (left == null || right == null) {
        throw new Error("strings must not be null");
    }
    if (threshold < 0) {
        throw new Error("threshold must not be negative");
    }

    /*
     * This implementation only computes the distance if it's less than or
     * equal to the threshold value, returning -1 if it's greater. The
     * advantage is performance: unbounded distance is O(nm), but a bound of
     * k allows us to reduce it to O(km) time by only computing a diagonal
     * stripe of width 2k + 1 of the cost table. It is also possible to use
     * this to compute the unbounded Levenshtein distance by starting the
     * threshold at 1 and doubling each time until the distance is found;
     * this is O(dm), where d is the distance.
     *
     * One subtlety comes from needing to ignore entries on the border of
     * our stripe eg. p[] = |#|#|#|* d[] = *|#|#|#| We must ignore the entry
     * to the left of the leftmost member We must ignore the entry above the
     * rightmost member
     *
     * Another subtlety comes from our stripe running off the matrix if the
     * strings aren't of the same size. Since string s is always swapped to
     * be the shorter of the two, the stripe will always run off to the
     * upper right instead of the lower left of the matrix.
     *
     * As a concrete example, suppose s is of length 5, t is of length 7,
     * and our threshold is 1. In this case we're going to walk a stripe of
     * length 3. The matrix would look like so:
     *
     * <pre>
     *    1 2 3 4 5
     * 1 |#|#| | | |
     * 2 |#|#|#| | |
     * 3 | |#|#|#| |
     * 4 | | |#|#|#|
     * 5 | | | |#|#|
     * 6 | | | | |#|
     * 7 | | | | | |
     * </pre>
     *
     * Note how the stripe leads off the table as there is no possible way
     * to turn a string of length 5 into one of length 7 in edit distance of
     * 1.
     *
     * Additionally, this implementation decreases memory usage by using two
     * single-dimensional arrays and swapping them back and forth instead of
     * allocating an entire n by m matrix. This requires a few minor
     * changes, such as immediately returning when it's detected that the
     * stripe has run off the matrix and initially filling the arrays with
     * large values so that entries we don't compute are ignored.
     *
     * See Algorithms on Strings, Trees and Sequences by Dan Gusfield for
     * some discussion.
     */

    let n = left.length; // length of left
    let m = right.length; // length of right

    // if one string is empty, the edit distance is necessarily the length
    // of the other
    if (n == 0) {
        return m <= threshold ? m : -1;
    } else if (m == 0) {
        return n <= threshold ? n : -1;
    }

    if (n > m) {
        // swap the two strings to consume less memory
        let tmp = left;
        left = right;
        right = tmp;
        n = m;
        m = right.length;
    }

    // the edit distance cannot be less than the length difference
    if (m - n > threshold) {
        return -1;
    }

    let p = new Array(n + 1); // 'previous' cost array, horizontally
    let d = new Array(n + 1); // cost array, horizontally
    let tempD; // placeholder to assist in swapping p and d

    let boundary = Math.min(n, threshold) + 1;
    for (let i = 0; i < boundary; ++i) {
        p[i] = i;
    }
    // these fills ensure that the value above the rightmost entry of our
    // stripe will be ignored in following loop iterations
    fill_array(p, boundary, p.length, MAX_VALUE);
    fill_whole_array(d, MAX_VALUE);

    // iterates through t
    for (let j = 1; j <= m; ++j) {
        let rightJ = right[j - 1]; // jth character of right
        d[0] = j;

        // compute stripe indices, constrain to array size
        let min = Math.max(1, j - threshold);
        let max = j > MAX_VALUE - threshold ? n : Math.min(
                n, j + threshold);

        // ignore entry left of leftmost
        if (min > 1) {
            d[min - 1] = MAX_VALUE;
        }

        // iterates through [min, max] in s
        for (let i = min; i <= max; ++i) {
            if (left[i - 1] == rightJ) {
                // diagonally left and up
                d[i] = p[i - 1];
            } else {
                // 1 + minimum of cell to the left, to the top, diagonally
                // left and up
                d[i] = 1 + Math.min(Math.min(d[i - 1], p[i]), p[i - 1]);
            }
        }

        // copy current distance counts to 'previous row' distance counts
        tempD = p;
        p = d;
        d = tempD;
    }

    // if p[n] is greater than the threshold, there's no guarantee on it
    // being the correct
    // distance
    if (p[n] <= threshold) {
        return p[n];
    }
    return -1;
}

function limited_compare_norm(left, right, threshold) {
    const norm_left = normalize_domain(left);
    const norm_right = normalize_domain(right);

    console.log("Comparing \"" + norm_left + "\" with \"" + norm_right + "\"");

    return limited_compare(norm_left, norm_right, threshold);
}

function addClickHandler(link, fallbackUrl) {
    var origUrl = link.href || fallbackUrl;
    console.log("Load and add for " + origUrl);
    link.onclick = getClickHandler(link, origUrl);
    console.log("Loaded and added for " + origUrl);
}

//Retreives the fully qualified domain name from a URL
function extractHostname(url) {
    var hostname = new URL(url).hostname;

    console.log("URL \"" + url + "\" maps to \"" + hostname + "\"");

    return hostname;
}

//Updates our cookie hashtable and our Chrome local storage
//to indicate that we have visited a webpage
function setCookie(cookieName, value) {
    var query = {};

    query["" + cookieName] = value;

    cookies["" + cookieName] = value;

    if (isChrome) {
        chrome.storage.local.set(query, function() {});
    } else {
        browser.storage.local.set(query);
    }
}

function isLikelyPhishing(url) {
    for (let ithUrl in cookies) {
        // Check for no TLD differences  throw off comparisons.
        if (limited_compare_norm(url, ithUrl, CUTOFF) != -1) {
            return ithUrl;
        }
    }
    return null;
}

//Get value of cookie, if any.
function getCookieValue(cookieName) {
    /*var cookieString = "[ ";

    for (var c in cookies) {
        cookieString += "\"" + c + "\" ";
    }

    cookieString += " ]";

    console.log("Cookie String: \"" + cookieString + "\"");*/

    return cookies[cookieName];
}

//Get time (seconds) remaining in a learning period.
function timeInLearningPeriod() {
    var installTime = getCookieValue("install_time");

    return (learningPeriod - (Date.now() - installTime)) / 1000.0;
}

//Test whether we are in a learning period or not.
function inLearningPeriod() {
    var timeLeft = timeInLearningPeriod();

    if (timeLeft > 0) {
        return true;
    } else {
        return false;
    }
}

//Check whether we have received an email from this email address or not recently.
function cookieExists(cookieName) {
    return getCookieValue(cookieName) != null;
}

//Returns a function that checks to see if a link's FQDN appears in our list of
//visited pages and asks the user for confirmation if it does not.
function getClickHandler(link1, oldUrl) {
    return function (e) {
        const domainName = extractHostname(link1.href || oldUrl);
        var response = true;

        if (!cookieExists(domainName) &&
            !inLearningPeriod()) {

            let similarPreviousUrl = isLikelyPhishing(domainName);

            if (similarPreviousUrl == null) {
                let message = "You have not been to \"" + domainName +
                "\" recently. If you recognize that website, then click \"Ok\"." +
                " Otherwise, click \"Cancel\" to stay on the current page.";

                response = window.confirm(message);
            } else {
                let message = "The domain of the link you just clicked on, \"" + domainName + "\", is very similar to a domain you previously visited, \"" + similarPreviousUrl + "\". Note these two domains are NOT the same and the one you clicked on is more likely than normal to redirect to a site you did not intend to visit. Are you sure you want to proceed?";

                response = window.confirm(message);
	    }
        }

        if (response) {
            setCookie(domainName, 1);
        }

        return response;
    };
}

//Goes through each link on the page and adds an onclick handler to it if one
//doesn't already exist and the link points to a different page. This handler
//will be responsible for checking whether the user has visited this page or not
//and asking the user for confirmation if the user has not visited this page.
function addHandlerToLinks() {
    console.log("Adding handler to links!");
    var links = document.getElementsByTagName("a");
    for (var i = 0; i < links.length; i += 1) {
        if (links[i].onclick == null) {
            console.log("Found link! " + links[i].href);
            addClickHandler(links[i], window.location.href);
        }
    }
}

//Sets a cookie if it does not already exist.
function setCookieIfNotExists(cookieName, value) {
    if (!cookieExists(cookieName)) {
        setCookie(cookieName, value);
    }
}

//Retrieves every item in our local storage. We add each item to our "cookies"
//object (dictionary/hashtable). This way, we will have a list of our
//previously-visited websites that we can reference synchronously.
function loadCookies() {
    //Get "items", an object that contains each item in our local storage.
    var getPromise = null;

    var callback = function (items) {
        for (var item in items) {
            var tempItem = "" + item; //Convert item to a string so that we don't try
            //to call undefined methods on an object.

            //Add items to our cookies dictionary.
            cookies[tempItem] = items[item];
	}

        setCookieIfNotExists("install_time", Date.now());
    };

    if (isChrome) {
        chrome.storage.local.get(null, callback);
    } else {
        getPromise = browser.storage.local.get(null);
        getPromise.then(callback);
    }
}

function validateIPAddress(ipAddress) {
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipAddress)) {
        return true;
    }
    return false;
}

function main() {
    //Extract FQDN from webpage URL.
    var domainName = extractHostname(window.location.href);

    //Add the current webpage's FQDN to the list of visited webpages.
    setCookie(domainName, 1);

    //Load history of visited sites from Chrome's local storage. This way, we
    //know which sites we have visited and we can compare domain names in links
    //against pages we have visited.
    loadCookies();

    console.log("Loaded cookies");

    //Create a periodic that will add click handlers to every new link on the
    //current webpage every 250/1000 = 1/4 second

    var handler = function () { addHandlerToLinks(); };

    window.setInterval(handler, 250);
}

main();
